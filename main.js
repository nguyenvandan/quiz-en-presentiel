const { Worker } = require('worker_threads');

const redisURL = {
    host: "127.0.0.1",
    port: '6379'
}

const event = {
    question: "QUESTION",
    answer: "ANSWER",
    start: "START",
    close: "CLOSE",
    closed: "CLOSED",
    feedback: "FEEBACK",
    ready: "USER_READY"
}

let adminSend = "AdminSendAll";
let adminGet = "AdminGetALL";

const threads = 2;

console.log("---------------Start program -----------");
console.log("---------------Nb Threads -----------: ", threads);

for (let i =0; i < threads; i++) {
    const port = new Worker(require.resolve('./user.js'), {
        workerData: { id: i, adminSend, adminGet, redisURL, event: event }
    })
    port.on("message", data => handleMessage(data, i));
    port.on("error", err => console.log(err));
    port.on("exit", code => console.log(`Exit code: ${code}`));
}
function handleMessage(_, index) {
    console.log(`Thread ${i}`)
}

const AdminPort = new Worker(require.resolve('./presenter.js'), {
    workerData: { adminSend, adminGet, redisURL, event}
})
AdminPort.on("message", data => handleMessage(data, i));
AdminPort.on("error", err => console.log(err));
AdminPort.on("exit", code => console.log(`Exit code: ${code}`));