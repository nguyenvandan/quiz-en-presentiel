const redis = require('redis');
const { parentPort, workerData } = require("worker_threads");
const { adminSend, adminGet, redisURL, event} = workerData;

const adminSub = redis.createClient(redisURL.port, redisURL.host);
const adminPub = redis.createClient(redisURL.port, redisURL.host);

// main channel all users => admin
adminSub.subscribe(adminGet);

adminSub.on("message", (channel, message) => {
    let msg = JSON.parse(message);
    console.log("user -> admin ", channel, " ", msg);
    if (msg && msg.event) {
        switch (msg.event) {
            case event.ready:
                let question =  "What is your name" ;
                adminPub.publish(adminSend, JSON.stringify({ event: event.question, data: { q: question } }) );
                break;
            case event.answer:
                (async () => {
                    await adminPub.publish(msg.data.ch, JSON.stringify({ event: event.feedback, data: { result: true } }) );
                    await adminPub.publish(msg.data.ch, JSON.stringify({ event: event.close, data: {} }) );
                })()
                break;
            default:
                console.log("admin default message: ", msg)
                break;
        }
    }
})

// main channel admin => all users
adminSub.on("subscribe", (privateCh, message) => {
    adminPub.publish(adminSend, JSON.stringify({ event: event.start, data: { f: 1 } }));
})
