# quiz-en-presentiel

Une démo d'un quiz en présentiel avec les participants: «utilisateurs» et «formateur» sous forme d’application Console pour Windows.

Le quiz est développé par Nodejs, dans lequel :

- **main.js** : le quiz
- **presenter.js** : la partie «formateur»
- **user.js** : la partie «utilisateur»

La partie «utilisateurs» est en multithreading, et chaque utilisateur gère sa propre connexion au serveur Redis.


## Pour lancer le quiz:

1. Installer Nodejs
1. Installer Redis serveur
1. Démarrer Redis serveur : **redis-server**
1. Démarrez le quiz : **node main.js**
