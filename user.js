const redis = require('redis');
const { parentPort, workerData } = require("worker_threads");
const { id, adminSend, adminGet, redisURL, event } = workerData;

let userPub = redis.createClient(redisURL.port, redisURL.host);
let userSub = redis.createClient(redisURL.port, redisURL.host);

let privateCh = `C${id}`;

// private chanel admin -> user
userSub.subscribe(privateCh);

// main channel admin -> all user
userSub.subscribe(adminSend);

userSub.on("subscribe", (channel, message) => {
    console.log(`user${id} join ${channel}`);
})

userSub.on("message", function(channel, message) {
    let msg = JSON.parse(message);
    console.log("admin -> user ", channel, " ", msg);
    if (msg && msg.event) {
        switch (msg.event) {
            case event.start:
                userPub.publish(adminGet, JSON.stringify({ event: event.ready, data: {t: 55} }) );
                break;
            case event.question:
                userPub.publish(adminGet, JSON.stringify({ event: event.answer, data: { a: "my answer", ch:  privateCh } }) );
                break; 
            case event.feeback:
                if (msg && msg.data) { console.log("my feeback: ", msg.data.feedback); }
                break; 
            case event.close:
                (async () => {
                    await userPub.publish(adminGet, JSON.stringify({ event: event.closed, data: {} }) );
                    await userPub.unsubscribe(adminSend);
                    await userPub.unsubscribe(privateCh);
                })()
                break; 
            default:
                console.log("user default message: ", msg)
                break;
        }
    }
})
